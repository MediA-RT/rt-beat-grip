// -*- C++ -*-
/*!
 * @file  SixLevelColourMapper.cpp
 * @brief Select one of the six colours depending on the input value
 * @date $Date$
 *
 * $Id$
 */

#include "SixLevelColourMapper.h"

// Module specification
// <rtc-template block="module_spec">
static const char* sixlevelcolourmapper_spec[] =
  {
    "implementation_id", "SixLevelColourMapper",
    "type_name",         "SixLevelColourMapper",
    "description",       "Select one of the six colours depending on the input value",
    "version",           "1.0.0",
    "vendor",            "ShintaroNakazawa",
    "category",          "Tools",
    "activity_type",     "PERIODIC",
    "kind",              "DataFlowComponent",
    "max_instance",      "1",
    "language",          "C++",
    "lang_type",         "compile",
    // Configuration variables
    "conf.default.min", "0",
    "conf.default.max", "100",
    // Widget
    "conf.__widget__.min", "text",
    "conf.__widget__.max", "text",
    // Constraints
    ""
  };
// </rtc-template>

/*!
 * @brief constructor
 * @param manager Maneger Object
 */
SixLevelColourMapper::SixLevelColourMapper(RTC::Manager* manager)
    // <rtc-template block="initializer">
  : RTC::DataFlowComponentBase(manager),
    m_ValueIn("Value", m_Value),
    m_ColourOut("Colour", m_Colour)

    // </rtc-template>
{
}

/*!
 * @brief destructor
 */
SixLevelColourMapper::~SixLevelColourMapper()
{
}



RTC::ReturnCode_t SixLevelColourMapper::onInitialize()
{
  // Registration: InPort/OutPort/Service
  // <rtc-template block="registration">
  // Set InPort buffers
  addInPort("Value", m_ValueIn);
  
  // Set OutPort buffer
  addOutPort("Colour", m_ColourOut);
  
  // Set service provider to Ports
  
  // Set service consumers to Ports
  
  // Set CORBA Service Ports
  
  // </rtc-template>

  // <rtc-template block="bind_config">
  // Bind variables and configuration variable
  bindParameter("min", m_min, "0");
  bindParameter("max", m_max, "100");
  // </rtc-template>
  
  return RTC::RTC_OK;
}

/*
RTC::ReturnCode_t SixLevelColourMapper::onFinalize()
{
  return RTC::RTC_OK;
}
*/

/*
RTC::ReturnCode_t SixLevelColourMapper::onStartup(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/

/*
RTC::ReturnCode_t SixLevelColourMapper::onShutdown(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/


RTC::ReturnCode_t SixLevelColourMapper::onActivated(RTC::UniqueId ec_id)
{
	//色の設定
	//level 0: blue
	//level 1: Skyblue
	//level 2: Green
	//level 3: Yellow
	//level 4: Orange
	//level 5: Red
	RTC::RGBColour clr[6] = {
		{ 0, 0, 255 }, { 0, 255, 255 }, { 0, 255, 0 }, { 255, 255, 0 }, { 255, 183, 76 }, { 255, 255, 0 }
	};

	for (int i = 0; i < 6; i++){
		colour[i] = clr[i];
	}

  return RTC::RTC_OK;
}

/*
RTC::ReturnCode_t SixLevelColourMapper::onDeactivated(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/

/***
受け取った値に応じた色を出力する

***/
RTC::ReturnCode_t SixLevelColourMapper::onExecute(RTC::UniqueId ec_id)
{
	double level_delta = (m_max - m_min) / 4;
	double ref = m_min;

	//ImPortのHeartに新たな値が入力されたとき
	if (m_ValueIn.isNew()){
		//新しい値を読み込み
		m_ValueIn.read();

		m_Colour.data = colour[5];
		for (int i = 0; i < 4; i++){
			if (m_Value.data <= ref){
				m_Colour.data = colour[i];
				break;
			}
			ref += level_delta;
		}

		//OutPortのColourにm_Colour.dataを出力
		m_ColourOut.write();
	}

  return RTC::RTC_OK;
}

/*
RTC::ReturnCode_t SixLevelColourMapper::onAborting(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/

/*
RTC::ReturnCode_t SixLevelColourMapper::onError(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/

/*
RTC::ReturnCode_t SixLevelColourMapper::onReset(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/

/*
RTC::ReturnCode_t SixLevelColourMapper::onStateUpdate(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/

/*
RTC::ReturnCode_t SixLevelColourMapper::onRateChanged(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/



extern "C"
{
 
  void SixLevelColourMapperInit(RTC::Manager* manager)
  {
    coil::Properties profile(sixlevelcolourmapper_spec);
    manager->registerFactory(profile,
                             RTC::Create<SixLevelColourMapper>,
                             RTC::Delete<SixLevelColourMapper>);
  }
  
};


