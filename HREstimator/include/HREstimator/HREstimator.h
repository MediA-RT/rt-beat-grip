// -*- C++ -*-
/*!
 * @file  HREstimator.h
 * @brief Estimate heart rate using photoreflector
 * @date  $Date$
 *
 * $Id$
 */

#ifndef HRESTIMATOR_H
#define HRESTIMATOR_H

#include <rtm/idl/BasicDataTypeSkel.h>
#include <rtm/idl/ExtendedDataTypesSkel.h>
#include <rtm/idl/InterfaceDataTypesSkel.h>

// Service implementation headers
// <rtc-template block="service_impl_h">

// </rtc-template>

// Service Consumer stub headers
// <rtc-template block="consumer_stub_h">

// </rtc-template>

// Service Consumer stub headers
// <rtc-template block="port_stub_h">
// </rtc-template>

using namespace RTC;

#include <rtm/Manager.h>
#include <rtm/DataFlowComponentBase.h>
#include <rtm/CorbaPort.h>
#include <rtm/DataInPort.h>
#include <rtm/DataOutPort.h>

/*!
 * @class HREstimator
 * @brief Estimate heart rate using photoreflector
 *
 */
class HREstimator
  : public RTC::DataFlowComponentBase
{
 public:
  /*!
   * @brief constructor
   * @param manager Maneger Object
   */
  HREstimator(RTC::Manager* manager);

  /*!
   * @brief destructor
   */
  ~HREstimator();

  // <rtc-template block="public_attribute">
  
  // </rtc-template>

  // <rtc-template block="public_operation">
  
  // </rtc-template>

  /***
   *
   * The initialize action (on CREATED->ALIVE transition)
   * formaer rtc_init_entry() 
   *
   * @return RTC::ReturnCode_t
   * 
   * 
   */
   virtual RTC::ReturnCode_t onInitialize();

  /***
   *
   * The finalize action (on ALIVE->END transition)
   * formaer rtc_exiting_entry()
   *
   * @return RTC::ReturnCode_t
   * 
   * 
   */
  // virtual RTC::ReturnCode_t onFinalize();

  /***
   *
   * The startup action when ExecutionContext startup
   * former rtc_starting_entry()
   *
   * @param ec_id target ExecutionContext Id
   *
   * @return RTC::ReturnCode_t
   * 
   * 
   */
  // virtual RTC::ReturnCode_t onStartup(RTC::UniqueId ec_id);

  /***
   *
   * The shutdown action when ExecutionContext stop
   * former rtc_stopping_entry()
   *
   * @param ec_id target ExecutionContext Id
   *
   * @return RTC::ReturnCode_t
   * 
   * 
   */
  // virtual RTC::ReturnCode_t onShutdown(RTC::UniqueId ec_id);

  /***
   *
   * The activated action (Active state entry action)
   * former rtc_active_entry()
   *
   * @param ec_id target ExecutionContext Id
   *
   * @return RTC::ReturnCode_t
   * 
   * 
   */
   virtual RTC::ReturnCode_t onActivated(RTC::UniqueId ec_id);

  /***
   *
   * The deactivated action (Active state exit action)
   * former rtc_active_exit()
   *
   * @param ec_id target ExecutionContext Id
   *
   * @return RTC::ReturnCode_t
   * 
   * 
   */
  // virtual RTC::ReturnCode_t onDeactivated(RTC::UniqueId ec_id);

  /***
   *
   * The execution action that is invoked periodically
   * former rtc_active_do()
   *
   * @param ec_id target ExecutionContext Id
   *
   * @return RTC::ReturnCode_t
   * 
   * 
   */
   virtual RTC::ReturnCode_t onExecute(RTC::UniqueId ec_id);

  /***
   *
   * The aborting action when main logic error occurred.
   * former rtc_aborting_entry()
   *
   * @param ec_id target ExecutionContext Id
   *
   * @return RTC::ReturnCode_t
   * 
   * 
   */
  // virtual RTC::ReturnCode_t onAborting(RTC::UniqueId ec_id);

  /***
   *
   * The error action in ERROR state
   * former rtc_error_do()
   *
   * @param ec_id target ExecutionContext Id
   *
   * @return RTC::ReturnCode_t
   * 
   * 
   */
  // virtual RTC::ReturnCode_t onError(RTC::UniqueId ec_id);

  /***
   *
   * The reset action that is invoked resetting
   * This is same but different the former rtc_init_entry()
   *
   * @param ec_id target ExecutionContext Id
   *
   * @return RTC::ReturnCode_t
   * 
   * 
   */
  // virtual RTC::ReturnCode_t onReset(RTC::UniqueId ec_id);
  
  /***
   *
   * The state update action that is invoked after onExecute() action
   * no corresponding operation exists in OpenRTm-aist-0.2.0
   *
   * @param ec_id target ExecutionContext Id
   *
   * @return RTC::ReturnCode_t
   * 
   * 
   */
  // virtual RTC::ReturnCode_t onStateUpdate(RTC::UniqueId ec_id);

  /***
   *
   * The action that is invoked when execution context's rate is changed
   * no corresponding operation exists in OpenRTm-aist-0.2.0
   *
   * @param ec_id target ExecutionContext Id
   *
   * @return RTC::ReturnCode_t
   * 
   * 
   */
  // virtual RTC::ReturnCode_t onRateChanged(RTC::UniqueId ec_id);


 protected:
  // <rtc-template block="protected_attribute">
  
  // </rtc-template>

  // <rtc-template block="protected_operation">
  
  // </rtc-template>

  // Configuration variable declaration
  // <rtc-template block="config_declare">
  /*!
   * 現在とその直前に取得したセンサ値の変化量がこれ以下ならほぼ変化
   * がないものとみなす
   * - Name: diffThreshold diffThreshold
   * - DefaultValue: 10
   */
  unsigned int m_diffThreshold;
  /*!
   * 立ち上がり中と判断するためのセンサ値の変化量の差の最小値
   * - Name: diffRisingMin diffRisingMin
   * - DefaultValue: 20
   */
  unsigned int m_diffRisingMin;
  /*!
   * 立ち上がり中と判断するためのセンサ値の変化量の差の最大値
   * - Name: diffRisingMax diffRisingMax
   * - DefaultValue: 280
   */
  unsigned int m_diffRisingMax;
  /*!
   * 立ち下がり中と判断するためのセンサ値の変化量の差の最小値
   * - Name: diffFallingMin diffFallingMin
   * - DefaultValue: 20
   */
  unsigned int m_diffFallingMin;
  /*!
   * 立ち下がり中と判断するためのセンサ値の変化量の差の最大値
   * - Name: diffFallingMax diffFallingMax
   * - DefaultValue: 200
   */
  unsigned int m_diffFallingMax;
  /*!
   * 立ち上がりと判断するための時間の最小値
   * - Name: timeRisingMin timeRisingMin
   * - DefaultValue: 50
   */
  unsigned int m_timeRisingMin;
  /*!
   * 立ち上がりと判断するための時間の最大値
   * - Name: timeRisingMax timeRisingMax
   * - DefaultValue: 400
   */
  unsigned int m_timeRisingMax;
  /*!
   * 立ち下がりと判断するための時間の最小値
   * - Name: timeFallingMin timeFallingMin
   * - DefaultValue: 96
   */
  unsigned int m_timeFallingMin;
  /*!
   * 立ち下がりと判断するための時間の最大値
   * - Name: timeFallingMax timeFallingMax
   * - DefaultValue: 800
   */
  unsigned int m_timeFallingMax;
  /*!
   * 立ち上がりと判断するための変化量の合計
   * - Name: diffTotalRisingThreshold diffTotalRisingThreshold
   * - DefaultValue: 160
   */
  unsigned int m_diffTotalRisingThreshold;
  /*!
   * 立ち下がりと判断するための変化量の合計
   * - Name: diffTotalFallingThreshold diffTotalFallingThreshold
   * - DefaultValue: 200
   */
  unsigned int m_diffTotalFallingThreshold;
  /*!
   * 推定に使用するデータ数
   * - Name: estimateDataNum estimateDataNum
   * - DefaultValue: 500
   */
  unsigned int m_estimateDataNum;
  /*!
   * 1サンプルごとの時間
   * - Name: sampleTime sampleTime
   * - DefaultValue: 10
   * - Unit: ms
   */
  unsigned int m_sampleTime;

  // </rtc-template>

  // DataInPort declaration
  // <rtc-template block="inport_declare">
  RTC::TimedDouble m_PhotoreflectorData;
  /*!
   */
  InPort<RTC::TimedDouble> m_PhotoreflectorDataIn;
  
  // </rtc-template>


  // DataOutPort declaration
  // <rtc-template block="outport_declare">
  RTC::TimedDouble m_HeartRate;
  /*!
   */
  OutPort<RTC::TimedDouble> m_HeartRateOut;
  
  // </rtc-template>

  // CORBA Port declaration
  // <rtc-template block="corbaport_declare">
  
  // </rtc-template>

  // Service declaration
  // <rtc-template block="service_declare">
  
  // </rtc-template>

  // Consumer declaration
  // <rtc-template block="consumer_declare">
  
  // </rtc-template>

 private:
  // <rtc-template block="private_attribute">
	int sensorValue;
	int integral_plus;
	int integral_minus;
	int count;
	int elapse_up;
	int elapse_down;
	int time_rate;

  // </rtc-template>

  // <rtc-template block="private_operation">
  
  // </rtc-template>

};


extern "C"
{
  DLL_EXPORT void HREstimatorInit(RTC::Manager* manager);
};

#endif // HRESTIMATOR_H
