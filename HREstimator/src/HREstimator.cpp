// -*- C++ -*-
/*!
 * @file  HREstimator.cpp
 * @brief Estimate heart rate using photoreflector
 * @date $Date$
 *
 * $Id$
 */

#include "HREstimator.h"

#include <cstdlib>

// Module specification
// <rtc-template block="module_spec">
static const char* hrestimator_spec[] =
  {
    "implementation_id", "HREstimator",
    "type_name",         "HREstimator",
    "description",       "Estimate heart rate using photoreflector",
    "version",           "1.0.0",
    "vendor",            "ShintaroNakazawa",
    "category",          "Tools",
    "activity_type",     "PERIODIC",
    "kind",              "DataFlowComponent",
    "max_instance",      "1",
    "language",          "C++",
    "lang_type",         "compile",
    // Configuration variables
    "conf.default.diffThreshold", "10",
    "conf.default.diffRisingMin", "20",
    "conf.default.diffRisingMax", "280",
    "conf.default.diffFallingMin", "20",
    "conf.default.diffFallingMax", "200",
    "conf.default.timeRisingMin", "50",
    "conf.default.timeRisingMax", "400",
    "conf.default.timeFallingMin", "96",
    "conf.default.timeFallingMax", "800",
    "conf.default.diffTotalRisingThreshold", "160",
    "conf.default.diffTotalFallingThreshold", "200",
    "conf.default.estimateDataNum", "500",
    "conf.default.sampleTime", "10",
    // Widget
    "conf.__widget__.diffThreshold", "text",
    "conf.__widget__.diffRisingMin", "text",
    "conf.__widget__.diffRisingMax", "text",
    "conf.__widget__.diffFallingMin", "text",
    "conf.__widget__.diffFallingMax", "text",
    "conf.__widget__.timeRisingMin", "text",
    "conf.__widget__.timeRisingMax", "text",
    "conf.__widget__.timeFallingMin", "text",
    "conf.__widget__.timeFallingMax", "text",
    "conf.__widget__.diffTotalRisingThreshold", "text",
    "conf.__widget__.diffTotalFallingThreshold", "text",
    "conf.__widget__.estimateDataNum", "text",
    "conf.__widget__.sampleTime", "text",
    // Constraints
    "conf.__constraints__.estimateDataNum", "x>0",
    "conf.__constraints__.sampleTime", "x>0",
    ""
  };
// </rtc-template>

/*!
 * @brief constructor
 * @param manager Maneger Object
 */
HREstimator::HREstimator(RTC::Manager* manager)
    // <rtc-template block="initializer">
  : RTC::DataFlowComponentBase(manager),
    m_PhotoreflectorDataIn("PhotoreflectorData", m_PhotoreflectorData),
    m_HeartRateOut("HeartRate", m_HeartRate)

    // </rtc-template>
{
}

/*!
 * @brief destructor
 */
HREstimator::~HREstimator()
{
}



RTC::ReturnCode_t HREstimator::onInitialize()
{
  // Registration: InPort/OutPort/Service
  // <rtc-template block="registration">
  // Set InPort buffers
  addInPort("PhotoreflectorData", m_PhotoreflectorDataIn);
  
  // Set OutPort buffer
  addOutPort("HeartRate", m_HeartRateOut);
  
  // Set service provider to Ports
  
  // Set service consumers to Ports
  
  // Set CORBA Service Ports
  
  // </rtc-template>

  // <rtc-template block="bind_config">
  // Bind variables and configuration variable
  bindParameter("diffThreshold", m_diffThreshold, "10");
  bindParameter("diffRisingMin", m_diffRisingMin, "20");
  bindParameter("diffRisingMax", m_diffRisingMax, "280");
  bindParameter("diffFallingMin", m_diffFallingMin, "20");
  bindParameter("diffFallingMax", m_diffFallingMax, "200");
  bindParameter("timeRisingMin", m_timeRisingMin, "50");
  bindParameter("timeRisingMax", m_timeRisingMax, "400");
  bindParameter("timeFallingMin", m_timeFallingMin, "96");
  bindParameter("timeFallingMax", m_timeFallingMax, "800");
  bindParameter("diffTotalRisingThreshold", m_diffTotalRisingThreshold, "160");
  bindParameter("diffTotalFallingThreshold", m_diffTotalFallingThreshold, "200");
  bindParameter("estimateDataNum", m_estimateDataNum, "500");
  bindParameter("sampleTime", m_sampleTime, "10");
  // </rtc-template>
  
  return RTC::RTC_OK;
}

/*
RTC::ReturnCode_t HREstimator::onFinalize()
{
  return RTC::RTC_OK;
}
*/

/*
RTC::ReturnCode_t HREstimator::onStartup(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/

/*
RTC::ReturnCode_t HREstimator::onShutdown(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/


RTC::ReturnCode_t HREstimator::onActivated(RTC::UniqueId ec_id)
{
	sensorValue = -1;
	integral_plus = 0;
	integral_minus = 0;
	count = 0;
	elapse_up = 0;
	elapse_down = 0;
	time_rate = 0;

	return RTC::RTC_OK;
}

/*
RTC::ReturnCode_t HREstimator::onDeactivated(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/

/***
センサーで取得した値から心拍数を求めるプログラム

当プログラムは心拍を検出する部分と心拍数を計算する部分に分かれている。

<心拍を検出する部分>
センサーで取得した最新の値とその直前の値との変化量から心拍を検出し、
検出するたびにint型変数countの値を1ずつ増やしていく(初期値は0)。
検出方法については下記サイトより引用した。
https://tokyodevices.jp/system/attachments/files/000/000/004/original/APS05K_MANUAL.pdf?1379525084

<心拍数を計算する部分>
計測ごとにint型変数time_rate値を1ずつ増やしていく(初期値は0)。
time_rateの値が指定回数以上になったとき、つまり（サンプルにかかる時間）x（指定回数）時間経過したとき心拍数を計算し始める。
計算方法としては、countの値に60/（経過時間）をかける。
そうすると60s間に心拍する回数、つまり心拍数の値が概算できる。

以上が心拍数計測のプロセスである。
***/
RTC::ReturnCode_t HREstimator::onExecute(RTC::UniqueId ec_id)
{
	int diff;

	if (m_PhotoreflectorDataIn.isNew()){
		m_PhotoreflectorDataIn.read();

		if (sensorValue < 0){ //初回はデータ蓄積だけ
			sensorValue = m_PhotoreflectorData.data;
			return RTC::RTC_OK;
		}

		/***
		心拍を検出する部分
		main code from https://tokyodevices.jp/system/attachments/files/000/000/004/original/APS05K_MANUAL.pdf?1379525084
		***/
		//最新とその直前に取得したセンサーの値の変化量をdiffとする
		//m_Sensor.data:最新のセンサーの値
		//sensorValue:直前のセンサーの値
		diff = std::abs(m_PhotoreflectorData.data - sensorValue);

		// 変化量が 0 に近く、この時点までの積分値が一定量であれば心拍の候補として扱う
		if (diff < m_diffThreshold && integral_plus > m_diffTotalRisingThreshold && integral_minus > m_diffTotalFallingThreshold) {
			// 立ち上がり時間とたち下がり時間を調べて、人間味のある数値になっていれば、心拍としてみなす。
			if (elapse_up > m_timeRisingMin && elapse_up < m_timeRisingMax && elapse_down > m_timeFallingMin && elapse_down < m_timeFallingMax) {
				// 心拍が検出されるとここに処理が移る
				count++;  //心拍のカウント
			}

			// クリア
			elapse_up = 0;
			elapse_down = 0;
			integral_plus = 0;
			integral_minus = 0;
		}

		else if (m_PhotoreflectorData.data > sensorValue && diff > m_diffRisingMin && diff < m_diffRisingMax) {
			// 波形の立ち上がりを検出
			integral_plus += diff;
			elapse_up += m_sampleTime;
		}

		else if (m_PhotoreflectorData.data < sensorValue && diff > m_diffFallingMin && diff < m_diffFallingMax) {
			// 波形の立下りを検出
			integral_minus += diff;
			elapse_down += m_sampleTime;
		}

		/***
		心拍数を計算する部分
		***/
		//m_sampleTime [ms] ごとにtime_rateをカウントする
		Sleep(m_sampleTime);
		time_rate++;


		//一定時間ごとに心拍数の計測をする
		if (time_rate >= m_estimateDataNum){
			int total_time = m_estimateDataNum*m_sampleTime;
			if (total_time == 0){
				std::cerr << "Invalid configuration value" << std::endl;
				return RTC::RTC_ERROR;
			}
			m_HeartRate.data = count * 60.0 * 1000 / total_time;
			std::cout << "HeartRate = " << m_HeartRate.data << std::endl << std::endl;

			//次の計測に向けてカウンターを初期化する
			count = 0;
			time_rate = 0;

			//心拍数を出力する
			m_HeartRateOut.write();
		}

		sensorValue = m_PhotoreflectorData.data;
	}

  return RTC::RTC_OK;
}

/*
RTC::ReturnCode_t HREstimator::onAborting(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/

/*
RTC::ReturnCode_t HREstimator::onError(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/

/*
RTC::ReturnCode_t HREstimator::onReset(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/

/*
RTC::ReturnCode_t HREstimator::onStateUpdate(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/

/*
RTC::ReturnCode_t HREstimator::onRateChanged(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/



extern "C"
{
 
  void HREstimatorInit(RTC::Manager* manager)
  {
    coil::Properties profile(hrestimator_spec);
    manager->registerFactory(profile,
                             RTC::Create<HREstimator>,
                             RTC::Delete<HREstimator>);
  }
  
};


